const form = document.getElementById('form');
const usuario = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

form.addEventListener('submit', e => {
    e.preventDefault();
    checkInput();
    
})

function checkInput(){
    const usuarioValue = usuario.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim();
    const password2Value = password2.value.trim();

    if (usuarioValue === ''){
        setErrorFor(usuario, 'No puede dejar el usuario en blanco.');
    }else{
        setSuccessFor(usuario);
    }

    if (emailValue === ''){
        setErrorFor(email, 'No puede dejar el email en blanco.');
    }else if(!isEmail(emailValue)){
        setErrorFor(email, 'No ingreso in email vañido.');
    }else{
        setSuccessFor(email);
    }

    if (passwordValue === ''){
        setErrorFor(password, 'No puede dejar el password en blanco.');
    }else{
        setSuccessFor(password);
    }

    if(password2Value === '') {
		setErrorFor(password2, 'Password 2 no puede estar en blanco');
	} else if(passwordValue !== password2Value) {
		setErrorFor(password2, 'Passwords no coinciden');
	} else{
		setSuccessFor(password2);
	}
}



function setErrorFor(input, message){
    const inputBox = input.parentElement;
    const small = inputBox.querySelector('small');
    inputBox.className = 'input-box error';
    small.innerText = message;
}

function setSuccessFor(input){
    const inputBox = input.parentElement;
    inputBox.className = 'input-box success';
}

function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}